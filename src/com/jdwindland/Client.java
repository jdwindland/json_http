package com.jdwindland;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Client {

    //create a new person object
    public static Person newPerson() {
        Person person = new Person();
        person.setName("Jennifer Windland");
        person.setGender('F');
        person.setAge(44);
        person.setAddress("108 Fay Street, Deer Creek, IL 61733");
        return person;
    }

    //get JSON string from server
    public static String getJSON(String string) {
        String response = null;
        try{
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while  ((line = reader.readLine()) !=null){
                stringBuilder.append (line);
            }
            response = stringBuilder.toString();
        }catch(Exception e){
            System.err.println(e.toString());
        }
        return response;
    }

    //Convert JSON string to Object
    public static Person toPerson(String string) throws JsonProcessingException {
        System.out.println("The Person object converted from JSON.\n");
        ObjectMapper mapper = new ObjectMapper();
        Person person;
        person = mapper.readValue(string, Person.class);
        return person;
    }
}
