package com.jdwindland;

import java.io.IOException;
import com.sun.net.httpserver.HttpServer;

public class Main {

    public static void main(String[] args) {

        try {
            //Start the server
            HttpServer server = Server.serverStart();

            //Get the JSON string from the server
            String json = Client.getJSON("http://localhost:8500/");

            //Takes JSON string and converts it to an object
            Person person = Client.toPerson(json);
            System.out.println(person + "\n");

            //Stop server and close connections - note: I commented this out to get screenshot showing JSON string on server
            String closed = Server.serverStop(server);
            System.out.println(closed);
        } catch(IllegalArgumentException | NullPointerException | IOException e1){
            System.out.println("There is a problem with the program.\nContact the developer for assistance.");
            System.err.println(e1.toString());
        }
    }
}